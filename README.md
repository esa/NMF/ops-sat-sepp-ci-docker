# Docker for SEPP CI 
Docker image based on ubuntu:16.04 containing a set of tools allowing to build SEPP packages (i.e. Linux-EABI Poky ARMv7 GCC, IPK build script).

## How to build
Builds are performed by Gitlab CI. However, manual build can be invoked by cloning this repository and running `DOCKER_BUILDKIT=1 docker build . -t sepp-ci:latest`

## How to run
The build assembled by Gitlab CI shall appear in the registry. Follow the associated guide:
https://about.gitlab.com/2016/05/23/gitlab-container-registry/

The build assembled locally can be run using `docker run sepp-ci:latest <command>`

